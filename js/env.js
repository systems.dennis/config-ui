class Environment {
    static self = "http://localhost:63342/config-ui/";
    static authApi = "https://auth.dennis.systems";
    static dataApi = "https://config.hlprod.ru";
    static authScope = "test_flaw";
    static scriptServer = Environment.self + 'js/';
    static defaultLang = "ru";
    static version = "1.98";
    static FORCE_PROCESSOR = false;
    static serverID = "flaw_test.localhost";
    static defaultIconSize = 32;
    static fileStorageServer = "https://files.dennis.systems/";
    static fileStorageApi = Environment.fileStorageServer + "api/v2/files/";
    static defaultTemplate = "themes/white/";
    static PROCESSOR_URL = Environment.fileStorageServer + "css/get/";
    static PROCESSOR_UPLOAD_URL = Environment.fileStorageServer + "css/compile/";
    static PROCESSOR_EXISTS_URL = Environment.fileStorageServer + "css/exists/";

    static formButtons = {'close': true, 'close_and_save':true, 'save':false};
    static LOG_LEVEL = -4;
    //("y-MM-dd-E" => 2024-05-04-Thu)
    //("y-M-d-E" => 2024-5-4-Thu)
    // y - Year
    // M - Month in year
    // d - Day in month
    // E - Day in week
    // a - Am/pm marker
    // k(+nn|-nn) - Hour in day (1 - 24)
    // h(+nn|-nn) - Hour in am/pm (1 - 12)
    // m - Minute in hour
    //More details https://github.com/noahcooper/SimpleDateFormatJS

    static formButtons = {'close': true, 'close_and_save':true, 'save':false};
    static LOG_LEVEL = -4;
    static scripts = [];
    static styles = ["css/toast.css", "css/issue.css","css/plugins/flow.css"];
    static themes = ["dark", "white"];

    static isNotRegisterLoginOrReset() {
        let path = location.protocol + "//" + location.host + location.pathname;
        return (
            path.indexOf("/client_login") == -1 &&
            path.indexOf("/restore") == -1 &&
            path.indexOf("/restore") == -1 &&
            path.indexOf("/register") == -1 &&
            path.indexOf("/verify/verification/verify_scope/") == -1 &&
            path.indexOf("/invitation/info") == -1&&
            path.indexOf("/forgot_password") == -1

        );
    }

    static addFiles() {
        if (Environment.isNotRegisterLoginOrReset()) {
            Environment.styles.push(
                "css/suneditor.min.css",
                "css/menu/menu_28102022.css"
            )
        } else {
            Environment.styles.push("css/menu/login_out_reg_pw_form.css");
        }
    }

    static initLoader(path, onLoad) {
        const head = document.getElementsByTagName("html")[0];

        const script = document.createElement("script");
        script.async = true;
        script.src = Environment.scriptServer + path;
        script.type = "text/javascript";

        script.onload = onLoad;

        head.prepend(script);
    }
}

Environment.addFiles();
Environment.initLoader("env-uploader.js", () =>{EnvironmentUploader.loadStyles()}, false);

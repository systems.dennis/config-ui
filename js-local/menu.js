function toggleMenu() {
    let isMenuOpen = true;

    const menu = h.fromFirst(".vertical_menu");
    const btnToggle = h.fromFirst(".toggle-menu");
    const btnImg = h.fromId('toggle-menu-icon');


    btnToggle.click(() => {

        menu.toggle("open-menu");
        if (isMenuOpen) {
            btnImg.src("menu_open.svg");
        } else {
            btnImg.src("menu_close.svg");
        }
        isMenuOpen = !isMenuOpen;
    });
}

function showMenu() {
    const menu = h.fromFirst(".vertical_menu");
    let column = menu.first()

    const btnImg = h.fromFirst(".toggle-menu").first()
    menu.toggle("open-menu");
    btnImg.src("menu_close.svg");

    setTimeout(() => {
        column.eachOf(((el) => {
            if (el.tagName) {
                el.classList.toggle('noTransition');
                menu.toggle('noTransition');
            }
        }))
    }, 0);
}


const observer = new MutationObserver(() => {
    if (h?.fromFirst(".vertical_menu")) {
        toggleMenu();
        observer.disconnect();
    }
});

observer.observe(document.body, {
    childList: true,
    subtree: true,
});